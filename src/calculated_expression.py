class CalculatedExpression:
    def __init__(self) -> None:
        self._result: float | None = None
        self._message: str = ""

    def __str__(self) -> str:
        if self._result is None:
            return self._message
        else:
            if self._is_result_long():
                return f"{self._result:4e}"
            if self._is_float():
                return str(self._result)
            else:
                return str(int(self._result))

    def _is_result_long(self) -> bool:
        return self._result > 1e8

    def _is_float(self) -> bool:
        return self._result % 1 != 0

    def _is_nan(self) -> bool:
        return self._result != self._result

    def update(self, value: float | None) -> None:
        self._result = value
        self._message = ""

    def set_message(self, msg: str) -> None:
        self._message = msg
        self._result = None

    def is_ok(self) -> bool:
        return isinstance(self._result, float)
