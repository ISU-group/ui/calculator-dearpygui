import dearpygui.dearpygui as dpg
import re

import calculator as calc

from .calculated_expression import CalculatedExpression


class Calculator:
    _char_to_keys = {
        "*": [dpg.mvKey_Multiply, 844],
        "/": [dpg.mvKey_Divide, 559],
        "+": [dpg.mvKey_Plus, 846],
        "-": [dpg.mvKey_Minus, 845],
        "<": [dpg.mvKey_Back],
        ".": [dpg.mvKey_Decimal, 558],
        "9": [dpg.mvKey_9, dpg.mvKey_NumPad9],
        "8": [dpg.mvKey_8, dpg.mvKey_NumPad8],
        "7": [dpg.mvKey_7, dpg.mvKey_NumPad7],
        "6": [dpg.mvKey_6, dpg.mvKey_NumPad6],
        "5": [dpg.mvKey_5, dpg.mvKey_NumPad5],
        "4": [dpg.mvKey_4, dpg.mvKey_NumPad4],
        "3": [dpg.mvKey_3, dpg.mvKey_NumPad3],
        "2": [dpg.mvKey_2, dpg.mvKey_NumPad2],
        "1": [dpg.mvKey_1, dpg.mvKey_NumPad1],
        "0": [dpg.mvKey_0, dpg.mvKey_NumPad0],
    }

    _key_combinations = {
        (dpg.mvKey_Shift, (dpg.mvKey_8, "8")): "*",
        (dpg.mvKey_Shift, (dpg.mvKey_6, "6")): "^",
        (dpg.mvKey_Shift, (dpg.mvKey_5, "5")): "%",
    }

    _input_field_tag = "input_field"
    _output_field_tag = "output_field"

    def __init__(self, calculator: calc.Calculator, window_name) -> None:
        self._calculator = calculator
        self._result = CalculatedExpression()
        self.window_name = window_name

        self._show()

    def _register_handlers(self) -> None:
        on_button_pressed = lambda sender, app_data, user_data: self._on_button_pressed(
            sender, app_data, user_data
        )

        on_calculate = lambda sender, app_data, user_data: self._on_calculate(
            sender, app_data, user_data
        )

        with dpg.handler_registry():
            for (char, keys) in Calculator._char_to_keys.items():
                for key in keys:
                    dpg.add_key_press_handler(
                        key=key,
                        callback=on_button_pressed,
                        user_data=char,
                    )

            dpg.add_key_press_handler(key=dpg.mvKey_Return, callback=on_calculate)

    def _on_button_pressed(self, sender, app_data, user_data) -> None:
        char = user_data

        if kc_value := self._get_key_combination_value(app_data, char):
            char = kc_value

        if self._is_remove_char(char):
            self._on_remove_char()
        elif self._is_calculate_char(char):
            self._on_calculate()
        elif self._is_allowed_char(char):
            self._on_add_char(char)

    def _get_key_combination_value(self, app_data, user_data) -> str | None:
        for (lhs_key, rhs_key) in Calculator._key_combinations:
            if dpg.is_key_down(lhs_key) and (app_data, user_data) == rhs_key:
                return Calculator._key_combinations[(lhs_key, rhs_key)]

        return None

    def _is_remove_char(self, char: str) -> bool:
        return char == "<"

    def _on_remove_char(self):
        text = dpg.get_value(Calculator._input_field_tag)
        self._update_io_field(Calculator._input_field_tag, text[:-1])

    def _is_calculate_char(self, char: str) -> bool:
        return char == "="

    def _on_calculate(self, sender=None, app_data=None, user_data=None):
        text = dpg.get_value(Calculator._input_field_tag)

        if text:
            try:
                self._result.update(self._calculator.calculate(text))
                self._clear_input_field()
            except ZeroDivisionError:
                self._result.set_message("Infinity")
                self._clear_input_field()
            except OverflowError:
                self._result.set_message("Number is too large")
            except Exception as e:
                self._result.set_message("Error")

            self._show_result()

    def _clear_input_field(self):
        self._update_io_field(Calculator._input_field_tag, "")

    def _update_io_field(self, tag, value):
        dpg.set_value(tag, value)

    def _show_result(self):
        self._update_io_field(Calculator._output_field_tag, str(self._result))

    def _is_allowed_char(self, char: str) -> bool:
        return bool(re.match(r"[\d\.\+\-\*\/\^\%]", char))

    def _on_add_char(self, char: str):
        is_operator = self._is_operator(char)
        text = dpg.get_value(Calculator._input_field_tag)

        if text:
            text += char
        elif not is_operator:
            text = char
            self._clear_output_field()
        else:
            if self._result.is_ok():
                text = str(self._result) + char
                self._clear_output_field()

        self._update_io_field(Calculator._input_field_tag, text)

    def _is_operator(self, char: str) -> bool:
        return char in "+-*/^%"

    def _clear_output_field(self):
        self._update_io_field(Calculator._output_field_tag, "")

    def _show(self):
        dpg.create_context()

        self._create_window()
        self._register_handlers()

        dpg.create_viewport(
            title=self.window_name, width=196, height=300, resizable=False
        )
        dpg.setup_dearpygui()
        dpg.show_viewport()
        dpg.set_primary_window(self.window_name, True)
        dpg.start_dearpygui()
        dpg.destroy_context()

    def _create_window(self):
        on_button_pressed = lambda sender, app_data, user_data: self._on_button_pressed(
            sender, app_data, user_data
        )

        on_calculate = lambda sender, app_data, user_data: self._on_calculate(
            sender, app_data, user_data
        )

        on_clear = lambda sender, app_data, user_data: self._on_clear(
            sender, app_data, user_data
        )

        with dpg.window(label=self.window_name, tag=self.window_name):
            self._add_input_text(callback=on_calculate)
            self._add_output_text()

            self._add_matrix_button(
                on_calculate=on_calculate,
                on_button_pressed=on_button_pressed,
                on_clear=on_clear,
            )

    def _on_clear(self, sender=None, app_data=None, user_data=None):
        self._clear_output_field()
        self._clear_input_field()
        self._result.update(None)

    def _add_input_text(self, callback):
        dpg.add_input_text(
            tag=Calculator._input_field_tag, on_enter=True, callback=callback
        )
        dpg.set_item_width(Calculator._input_field_tag, -1)

    def _add_output_text(self):
        dpg.add_text(tag=Calculator._output_field_tag)

    def _add_matrix_button(
        self,
        on_calculate,
        on_button_pressed,
        on_clear,
    ):
        with dpg.table(header_row=False, resizable=False, width=-1, height=-1):

            dpg.add_table_column()
            dpg.add_table_column()
            dpg.add_table_column()
            dpg.add_table_column()

            buttons_value = ["C*/<", "789-", "456+", "123%", "^0.="]
            for i in range(0, 5):
                with dpg.table_row():
                    for char in buttons_value[i]:
                        callback_handler = on_button_pressed

                        if char == "=":
                            callback_handler = on_calculate
                        elif char == "C":
                            callback_handler = on_clear
                        dpg.add_button(
                            label=char,
                            width=40,
                            height=40,
                            user_data=char,
                            callback=callback_handler,
                        )
