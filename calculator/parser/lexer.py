from curses.ascii import isdigit
from .token import Token, TokenKind


_EOS_TOKEN = Token(TokenKind.eos, value="")


class LexerException(Exception):
    pass


class Lexer:
    _operatorToTokenKind = {
        "+": TokenKind.plus,
        "-": TokenKind.minus,
        "*": TokenKind.mul,
        "/": TokenKind.div,
        "^": TokenKind.pow,
        "%": TokenKind.percent,
        "(": TokenKind.lparen,
        ")": TokenKind.rparen,
    }

    def tokenize(self, text: str) -> None:
        self._position: int = -1
        self._current_char: str = ""
        self._text: str = text

        self._forward()

    def _forward(self) -> None:
        self._position += 1
        self._current_char = "" if self._has_read_all() else self._text[self._position]

    def _has_read_all(self) -> bool:
        return self._position >= len(self._text)

    def next(self) -> Token:
        while not self._is_current_char_empty():
            if self._is_current_char_whitespace():
                self._skip_whitespace()
                continue

            return self._get_next_token()

        return _EOS_TOKEN

    def _is_current_char_empty(self) -> bool:
        return self._current_char == ""

    def _is_current_char_whitespace(self) -> bool:
        return self._current_char == " "

    def _skip_whitespace(self):
        while self._is_current_char_whitespace():
            self._forward()

    def _get_next_token(self) -> Token:
        if self._is_current_char_empty():
            return _EOS_TOKEN

        if self._is_current_char_digit() or self._is_current_char_dot():
            return self._number()
        elif self._is_current_char_operator():
            return self._operator()
        else:
            raise LexerException(f"Undefined token: {self._current_char}")

    def _is_current_char_digit(self) -> bool:
        return self._current_char.isdigit()

    def _is_current_char_dot(self) -> bool:
        return self._current_char == "."

    def _number(self) -> Token:
        number_parts = [self._get_integer()]

        if self._is_current_char_dot():
            self._forward()
            number_parts.extend([".", self._get_integer()])

        if self._is_current_char_exp():
            number_parts.append(self._get_exponent_part())

        value = "".join(number_parts)
        return Token(TokenKind.number, value=value)

    def _get_integer(self) -> str:
        digits = []
        while self._is_current_char_digit():
            digits.append(self._current_char)
            self._forward()

        return "".join(digits)

    def _is_current_char_exp(self) -> bool:
        return self._current_char == "e"

    def _get_exponent_part(self) -> list[str]:
        exponent = ["e"]
        self._forward()

        if self._current_char in "+-":
            exponent.append(self._current_char)
            self._forward()

        if power := self._get_integer():
            exponent.append(power)
            return "".join(exponent)
        else:
            raise LexerException("Invalid expression")

    def _is_current_char_operator(self) -> bool:
        return self._current_char in Lexer._operatorToTokenKind

    def _operator(self) -> Token:
        token_kind = Lexer._operatorToTokenKind[self._current_char]
        token = Token(token_kind, value=self._current_char)

        self._forward()
        return token
