from abc import ABC
from .token import Token


class Node(ABC):
    def __str__(self) -> str:
        return self.__class__.__name__


class Number(Node):
    def __init__(self, token: Token) -> None:
        super().__init__()
        self.token: Token = token

    def __str__(self) -> str:
        return f"{super().__str__()}({self.token})"


class BinOp(Node):
    def __init__(self, lhs: Node, op: Token, rhs: Node) -> None:
        super().__init__()
        self.lhs = lhs
        self.op = op
        self.rhs = rhs

    def __str__(self) -> str:
        return f"<{self.op.value}>({self.lhs}, {self.rhs})"


class UnaryOp(Node):
    def __init__(self, node: Node, op: Token) -> None:
        super().__init__()
        self.node = node
        self.op = op

    def __str__(self) -> str:
        return f"<{self.op.value}>({self.node}"


class EosNode(Node):
    pass
