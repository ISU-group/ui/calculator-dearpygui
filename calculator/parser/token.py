from enum import Enum, auto


class TokenKind(Enum):
    number = auto()
    plus = auto()
    minus = auto()
    mul = auto()
    div = auto()
    pow = auto()
    percent = auto()
    lparen = auto()
    rparen = auto()
    dot = auto()
    eos = auto()


class Token:
    def __init__(self, kind: TokenKind, value: str) -> None:
        self.kind = kind
        self.value = value

    def __str__(self) -> str:
        return f"Token({self.type_}, {self.value})"

    def __repr__(self) -> str:
        return str(self)
