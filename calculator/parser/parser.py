from calculator.parser.token import TokenKind
from .lexer import Lexer, LexerException
from .node import BinOp, EosNode, Node, Number, UnaryOp


class ParserException(Exception):
    pass


class Parser:
    def __init__(self) -> None:
        self._lexer = Lexer()
        self._current_token = None

    def parse(self, text) -> Node:
        try:
            self._lexer.tokenize(text)
            self._current_token = self._lexer.next()

            return self._build_tree()
        except LexerException as e:
            raise ParserException(e)

    def _build_tree(self) -> Node:
        tree = self._expr()

        if self._has_parsable_token():
            raise ParserException("Invalid expression")

        return tree

    def _expr(self) -> Node:
        ops = [TokenKind.plus, TokenKind.minus]
        result = self._term()

        while self._current_token.kind in ops:
            token = self._current_token
            self._update_current_token_if_kind_eq(token.kind)
            result = BinOp(result, token, self._term())

        return result

    def _term(self) -> Node:
        result = self._pow_term()
        ops = [TokenKind.mul, TokenKind.div, TokenKind.percent]

        while self._current_token.kind in ops:
            token = self._current_token
            self._update_current_token_if_kind_eq(token.kind)
            result = BinOp(result, token, self._term())

        return result

    def _pow_term(self) -> Node:
        result = self._factor()
        ops = [TokenKind.pow]

        while self._current_token.kind in ops:
            token = self._current_token
            self._update_current_token_if_kind_eq(token.kind)
            result = BinOp(result, token, self._pow_term())

        return result

    def _factor(self) -> Node:
        if self._current_token is None:
            raise ParserException("No token supplied")

        token = self._current_token

        if token.kind == TokenKind.number:
            self._update_current_token_if_kind_eq(token.kind)
            return Number(token)
        elif token.kind == TokenKind.lparen:
            self._update_current_token_if_kind_eq(token.kind)
            result = self._expr()
            self._update_current_token_if_kind_eq(TokenKind.rparen)
            return result
        elif token.kind in [TokenKind.minus, TokenKind.plus]:
            self._update_current_token_if_kind_eq(token.kind)
            return UnaryOp(node=self._term(), op=token)
        elif token.kind == TokenKind.eos:
            return EosNode()
        else:
            raise ParserException(f"Invalid token: {token}")

    def _update_current_token_if_kind_eq(self, token_kind: TokenKind) -> None:
        if self._current_token is None:
            raise ParserException("No token supplied")

        if self._current_token.kind == token_kind:
            self._current_token = self._lexer.next()
        else:
            raise ParserException(
                f"Invalid token type - expected({token_kind}), actual({self._current_token.kind}"
            )

    def _has_parsable_token(self) -> bool:
        return (
            self._current_token is not None
            and self._current_token.kind != TokenKind.eos
        )
