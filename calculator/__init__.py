import operator

from .parser.token import TokenKind
from .parser.node import *
from .parser.parser import Parser, ParserException


class CalculatorException(Exception):
    pass


class Calculator:
    def __init__(self, parser: Parser = Parser()) -> None:
        self._parser = parser

    def calculate(self, text: str) -> float:
        try:
            tree = self._parser.parse(text)
            return self._visit(tree)
        except ParserException:
            raise CalculatorException()

    def _visit(self, node: Node) -> float:
        if isinstance(node, Number):
            return self._visit_number(node)
        elif isinstance(node, BinOp):
            return self._visit_bin_op(node)
        elif isinstance(node, UnaryOp):
            return self._visit_unary_op(node)

        raise CalculatorException(f"Invalid node: {node}")

    def _visit_number(self, node: Number) -> float:
        return float(node.token.value)

    def _visit_bin_op(self, node: BinOp) -> float:
        binop = {
            TokenKind.plus: operator.add,
            TokenKind.minus: operator.sub,
            TokenKind.mul: operator.mul,
            TokenKind.div: operator.truediv,
            TokenKind.pow: operator.pow,
        }.get(node.op.kind)

        if binop:
            return binop(self._visit(node.lhs), self._visit(node.rhs))
        elif node.op.kind == TokenKind.percent:
            return self._visit(node.lhs) * self._visit(node.rhs) * 0.01

        raise CalculatorException(f"Invalid node (bin op): {node}")

    def _visit_unary_op(self, node: UnaryOp) -> float:
        unary_op = {
            TokenKind.plus: operator.pos,
            TokenKind.minus: operator.neg,
        }.get(node.op.kind)

        if unary_op:
            return unary_op(self._visit(node.node))

        raise CalculatorException(f"Invalid node (unary op): {node}")
